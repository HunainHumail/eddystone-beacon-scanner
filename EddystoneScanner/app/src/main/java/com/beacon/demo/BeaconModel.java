package com.beacon.demo;

public class BeaconModel {
    String beaconName;
    String distance;
    String rss;
    String beaconTx;
    String beaconURL;

    public String getBeaconName() {
        return beaconName;
    }

    public void setBeaconName(String beaconName) {
        this.beaconName = beaconName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBeaconRss() {
        return rss;
    }

    public void setBeaconRss(String rss) {
        this.rss=rss;
    }

    public String getBeaconTx() {
        return beaconTx;
    }

    public void setBeaconTx(String beaconTx) {
        this.beaconTx=beaconTx;
    }

    public String getBeaconUrl() {
        return beaconURL;
    }

    public void setBeaconURL(String beaconURL) {
        this.beaconURL=beaconURL;
    }




}
